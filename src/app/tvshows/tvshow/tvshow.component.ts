import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TVShow} from '../TVShow';

@Component({
  selector: 'media-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.scss']
})
export class TVShowComponent implements OnInit {


  @Input() private tvshow: TVShow;
  @Input() private actionText: string;
  @Output() private action: EventEmitter<TVShow> = new EventEmitter<TVShow>();

  constructor() {
  }

  ngOnInit() {
  }
}
