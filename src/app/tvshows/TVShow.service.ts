import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {TVShow} from './TVShow';

@Injectable({
  providedIn: 'root'
})
export class TVShowService {

  getTVShows(): Observable<TVShow[]> {
    return this.http.get<TVShow[]>(`${environment.apiUrl}/tvshows`);
  }

  getTVShow(id: number): Observable<TVShow> {
    return this.http.get<TVShow>(`${environment.apiUrl}/tvshows/${id}`);
  }

  addTVShow(id: number): Observable<TVShow> {
    return this.http.post<TVShow>(`${environment.apiUrl}/tvshows/watchlist`, {
      apiId: id,
    });
  }

  deleteTVShow(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/tvshows/${id}`);
  }

  search(searchQuery: string): Observable<TVShow[]> {
    if (searchQuery === '') {
      return;
    }

    return this.http.get<TVShow[]>(`${environment.apiUrl}/tvshows/search`, {
      params: new HttpParams().set('title', searchQuery),
    });
  }

  searchCollection(searchQuery: string): Observable<TVShow[]> {
    if (searchQuery === '') {
      return this.getTVShows();
    }

    return this.http.get<TVShow[]>(`${environment.apiUrl}/tvshows/search`, {
      params: new HttpParams().set('title', searchQuery).set('online', 'false'),
    });
  }
  constructor(private http: HttpClient) { }
}
