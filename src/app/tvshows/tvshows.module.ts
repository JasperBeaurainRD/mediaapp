import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TVShowsDetailsComponent} from './details/details.component';
import {TVShowsSearchComponent} from './search/search.component';
import {TVShowsWatchListComponent} from './watch-list/watch-list.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {TVShowComponent} from './tvshow/tvshow.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    TVShowsDetailsComponent,
    TVShowsSearchComponent,
    TVShowsWatchListComponent,
    TVShowComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    SharedModule,
  ]
})
export class TVShowsModule {
}
