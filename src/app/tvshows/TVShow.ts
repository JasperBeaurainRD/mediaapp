export interface TVShow {
  status: string;
  id?: number;
  onlineId?: string;
  title: string;
  poster: string;
  year: number;
  overview?: string;
  rating?: number;
  runtime?: number;
}
