import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {TVShow} from '../TVShow';
import {TVShowService} from '../TVShow.service';

@Component({
  selector: 'media-tvshows-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class TVShowsSearchComponent implements OnInit {


  private searchQuery$: Observable<string> = new Observable<string>();
  private results: TVShow[] = [];
  private searchError: boolean = false;
  private addError: boolean = false;
  private hasSearched: boolean = false;

  private searched(searchQuery: string): void {
    if (searchQuery === '') {
      return;
    }

    this.tvShowService.search(searchQuery).subscribe((results: TVShow[]) => {
      this.searchError = false;
      this.hasSearched = true;
      this.results = results;
    }, () => {
    }, () => {
      this.searchError = true;
    });
  }

  private add(id: number): void {
    this.tvShowService.addTVShow(id).subscribe((tvShow: TVShow) => {
      this.addError = false;
      this.router.navigate(['/tvshows', tvShow.id]);
    }, () => {
      this.addError = true;
      setTimeout(() => {
        this.addError = false;
      }, 2000);
    });
  }

  constructor(private tvShowService: TVShowService, private router: Router) {
  }

  ngOnInit(): void {
  }
}
