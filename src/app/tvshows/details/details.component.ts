import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TVShow} from '../TVShow';
import {TVShowService} from '../TVShow.service';

@Component({
  selector: 'media-tvshows-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class TVShowsDetailsComponent implements OnInit {

  private tvShow: TVShow;
  private loadingError: boolean = false;
  private deleteError: boolean = false;

  constructor(private route: ActivatedRoute, private tvShowService: TVShowService, private router: Router) {
  }

  ngOnInit() {
    this.tvShowService.getTVShow(Number(this.route.snapshot.paramMap.get('id'))).subscribe((tvShow: TVShow) => {
      this.tvShow = tvShow;
    }, () => {
      this.loadingError = true;
    });
  }

  private delete() {
    this.tvShowService.deleteTVShow(this.tvShow.id).subscribe(() => {
      this.deleteError = false;
      this.router.navigate(['/tvshows']);
    }, () => {
      this.loadingError = true;
      setTimeout(() => {
        this.deleteError = false;
      }, 2000);
    });
  }

}
