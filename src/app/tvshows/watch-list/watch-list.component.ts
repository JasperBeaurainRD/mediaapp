import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TVShowService} from '../TVShow.service';
import {TVShow} from '../TVShow';

@Component({
  selector: 'media-watch-list',
  templateUrl: './watch-list.component.html',
  styleUrls: ['./watch-list.component.scss']
})
export class TVShowsWatchListComponent implements OnInit {


  private tvShows: TVShow[] = [];
  private error: boolean = false;

  private viewDetails(tvShow: TVShow) {
    this.router.navigate(['/tvshows', tvShow.id]);
  }

  private searched(searchQuery: string) {
    if (searchQuery === '') {
      this
    }

    this.tvShowsService.searchCollection(searchQuery).subscribe((results: TVShow[]) => {
      this.error = false;
      this.tvShows = results;
    }, () => {
      this.error = true;
    });
  }

  constructor(private tvShowsService: TVShowService, private router: Router) {
  }

  ngOnInit() {
    this.tvShowsService.getTVShows().subscribe((tvShows: TVShow[]) => {
      this.tvShows = tvShows;
    }, () => {
      this.error = true;
    });
  }

}
