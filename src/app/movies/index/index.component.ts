import {Component, OnInit} from '@angular/core';
import {MovieService} from '../movies.service';
import {Movie} from '../Movie';
import {Router} from '@angular/router';

@Component({
  selector: 'media-movies-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class MoviesIndexComponent implements OnInit {

  private isPrivateDefault: boolean;
  private movies: Movie[] = [];
  private error: boolean = false;

  private viewDetails(movie: Movie) {
    this.router.navigate(['/movies', movie.id]);
  }

  private searched(searchQuery: string) {
    this.movieService.searchCollection(searchQuery).subscribe((results: Movie[]) => {
      this.error = false;
      this.movies = results;
    }, () => {
      this.error = true;
    });
  }

  private setPrivateCollection(isPrivate: boolean) {
    this.movieService.public = !isPrivate;
  }

  constructor(private movieService: MovieService, private router: Router) {}

  ngOnInit() {
    this.isPrivateDefault = !this.movieService.public;

    this.movieService.getMovies().subscribe((movies: Movie[]) => {
      this.movies = movies;
    }, () => {
      this.error = true;
    });
  }

}
