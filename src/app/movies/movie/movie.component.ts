import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Movie} from '../Movie';

@Component({
  selector: 'media-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  @Input() private movie: Movie;
  @Input() private actionText: string;
  @Output() private action: EventEmitter<Movie> = new EventEmitter<Movie>();

  constructor() { }

  ngOnInit() {
  }

}
