import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateMovieComponent} from './create/create.component';
import {MoviesIndexComponent} from './index/index.component';
import {MovieDetailComponent} from './detail/detail.component';
import {MovieComponent} from './movie/movie.component';
import {TestComponent} from '../observable-experiments/test/test.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    CreateMovieComponent,
    MoviesIndexComponent,
    MovieDetailComponent,
    MovieComponent,
    TestComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    SharedModule,
  ]
})
export class MoviesModule {
}
