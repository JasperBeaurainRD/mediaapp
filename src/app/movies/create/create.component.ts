import { Component, OnInit } from '@angular/core';
import {MovieService} from '../movies.service';
import {Router} from '@angular/router';
import {Movie} from '../Movie';
import {Observable} from 'rxjs';

@Component({
  selector: 'media-movies-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateMovieComponent implements OnInit {

  private isPrivateDefault: boolean;
  private searchQuery$: Observable<string> = new Observable<string>();
  private results: Movie[] = [];
  private searchError: boolean = false;
  private addError: boolean = false;
  private hasSearched: boolean = false;

  private searched(searchQuery: string): void {
    if (searchQuery === '') {
      return;
    }

    this.moviesService.searchOnline(searchQuery).subscribe((results: Movie[]) => {
      this.searchError = false;
      this.hasSearched = true;
      this.results = results;
    }, () => {
      this.searchError = true;
    });
  }

  private add(id: number): void {
    this.moviesService.addMovie(id).subscribe((movie: Movie) => {
      this.addError = false;
      this.router.navigate(['/movies', movie.id]);
    }, () => {
      this.addError = true;
      setTimeout(() => {
        this.addError = false;
      }, 2000);
    });
  }

  private setPrivateCollection(isPrivate: boolean): void {
    this.moviesService.public = !isPrivate;
  }

  constructor(private moviesService: MovieService, private router: Router) {}

  ngOnInit() {
    this.isPrivateDefault = !this.moviesService.public;
  }
}
