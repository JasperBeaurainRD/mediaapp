import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MovieService} from '../movies.service';
import {Movie} from '../Movie';

@Component({
  selector: 'media-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  private movie: Movie;
  private loadingError: boolean = false;
  private deleteError: boolean = false;

  constructor(private route: ActivatedRoute, private movieService: MovieService, private router: Router) { }

  ngOnInit() {
    this.movieService.getMovie(Number(this.route.snapshot.paramMap.get('id'))).subscribe((movie: Movie) => {
      this.movie = movie;
    }, () => {
      this.loadingError = true;
    });
  }

  private delete() {
    this.movieService.deleteMovie(this.movie.id).subscribe(() => {
      this.deleteError = false;
      this.router.navigate(['/movies']);
    }, () => {
      this.loadingError = true;
      setTimeout(() => {
        this.deleteError = false;
      }, 2000);
    });
  }
}
