import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Movie} from './Movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  public public: boolean = false;

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${environment.apiUrl}/movies`, {
      params: new HttpParams().set('public', `${this.public}`),
    });
  }

  getMovie(id: number): Observable<Movie> {
    return this.http.get<Movie>(`${environment.apiUrl}/movies/${id}`);
  }

  addMovie(id: number): Observable<Movie> {
    return this.http.post<Movie>(`${environment.apiUrl}/movies`, {
      apiId: id,
    }, {
      params: new HttpParams().set('public', `${this.public}`),
    });
  }

  deleteMovie(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/movies/${id}`);
  }


  searchOnline(searchQuery: string): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${environment.apiUrl}/movies/search`, {
      params: new HttpParams().set('title', searchQuery),
    });
  }

  searchCollection(searchQuery: string): Observable<Movie[]> {
    if (searchQuery === '') {
      return this.getMovies();
    }

    return this.http.get<Movie[]>(`${environment.apiUrl}/movies/search`, {
      params: new HttpParams().set('title', searchQuery).set('online', 'false').set('public', `${this.public}`),
    });
  }

  constructor(private http: HttpClient) { }
}
