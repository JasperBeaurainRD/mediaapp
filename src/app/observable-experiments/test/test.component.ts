import { Component, OnInit } from '@angular/core';
import {Observable, range, timer} from 'rxjs';

@Component({
  selector: 'media-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor() { }
  private num: number;
  private source: Observable<number> = range(0, 10);
  private interval: Observable<number> = timer(250, 1000);


  private ownTimer = (startTimeout: number, interval: number) => {
    return Observable.create((observer) => {
      let i = 0;

      const notify = () => {
        if (!observer.closed) {
          observer.next(i++);
          console.log('next called');
          setTimeout(notify, interval);
        }
      };

      setTimeout(notify, startTimeout);
    });
  };

  private ownRange = (start: number, end: number): Observable<number> => {
    return Observable.create((observer) => {
      for (let i = start; i < end; i++) {
        observer.next(i);
      }
    });
  };

  private ownInterval: Observable<number> = this.ownTimer( 250, 1000);
  private ownSource: Observable<number> = this.ownRange(0, 10);

  ngOnInit() {
    this.interval.subscribe((n: number) => {
      this.num = n;
    });
  }

}
