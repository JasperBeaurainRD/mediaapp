import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'media-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit, AfterViewInit {

  @Input() private onText: string = 'on';
  @Input() private offText: string = 'off';
  @Input() private default: boolean = false;
  @Output() private toggled: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('cb', {static: false}) private checkBox: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.checkBox.nativeElement.checked = this.default;
  }

}
