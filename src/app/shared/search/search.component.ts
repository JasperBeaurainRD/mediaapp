import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter} from 'rxjs/operators';

@Component({
  selector: 'media-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  @Input() private debounceTime: number = 100;
  private searchQuery$: Subject<string> = new Subject<string>();
  @Output() private searched: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.searchQuery$.pipe(
      // filter(searchQuery => searchQuery.length > 1)
      debounceTime(this.debounceTime),
      distinctUntilChanged(),
    ).subscribe((searchQuery: string): void => {
      this.searched.emit(searchQuery);
    });
  }

  ngOnDestroy(): void {
    this.searchQuery$.complete();
  }

}
