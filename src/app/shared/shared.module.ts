import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SwitchComponent} from './switch/switch.component';
import {SearchComponent} from './search/search.component';


@NgModule({
  declarations: [
    SwitchComponent,
    SearchComponent
  ],
  exports: [
    SwitchComponent,
    SearchComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule {
}
