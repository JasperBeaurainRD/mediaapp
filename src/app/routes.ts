import {Routes} from '@angular/router';
import {MoviesIndexComponent} from './movies/index/index.component';
import {MovieDetailComponent} from './movies/detail/detail.component';
import {CreateMovieComponent} from './movies/create/create.component';
import {ActorsOverviewComponent} from './actors/overview/overview.component';
import {ActorDetailComponent} from './actors/detail/detail.component';
import {CreateActorComponent} from './actors/create/create.component';
import {TestComponent} from './observable-experiments/test/test.component';
import {TVShowsWatchListComponent} from './tvshows/watch-list/watch-list.component';
import {TVShowsSearchComponent} from './tvshows/search/search.component';
import {TVShowsDetailsComponent} from './tvshows/details/details.component';

export const AppRoutes: Routes = [
  {
    path: 'movies',
    component: MoviesIndexComponent,
  },
  {
    path: 'movies/create',
    component: CreateMovieComponent,
  },
  {
    path: 'movies/:id',
    component: MovieDetailComponent,
  },
  {
    path: 'actors',
    component: ActorsOverviewComponent,
    children: [
      {
        path: 'create',
        component: CreateActorComponent,
      },
      {
        path: ':id',
        component: ActorDetailComponent,
      },
    ],
  },
  {
    path: 'observables',
    component: TestComponent,
  },
  {
    path: 'tvshows',
    component: TVShowsWatchListComponent,
  },
  {
    path: 'tvshows/search',
    component: TVShowsSearchComponent,
  },
  {
    path: 'tvshows/:id',
    component: TVShowsDetailsComponent,
  },
  {
    path: '',
    redirectTo: 'movies',
    pathMatch: 'full',
  },
];
