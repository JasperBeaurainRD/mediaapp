import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';

import {MainComponent} from './main/main.component';
import {HeaderComponent} from './header/header.component';
import {RouterModule} from '@angular/router';
import {AppRoutes} from './routes';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthorizationInterceptor} from './authorization.filter';
import {MoviesModule} from './movies/movies.module';
import {ActorsModule} from './actors/actors.module';
import {TVShowsModule} from './tvshows/tvshows.module';

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    MoviesModule,
    ActorsModule,
    TVShowsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }
  ],
  bootstrap: [MainComponent],
})
export class AppModule {
}
