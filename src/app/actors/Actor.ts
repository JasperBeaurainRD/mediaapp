export interface Actor {
  id?: number;
  name: string;
  desc: string;
  imagePath: string;
}

