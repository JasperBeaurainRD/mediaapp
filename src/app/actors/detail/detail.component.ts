import {Component, OnInit} from '@angular/core';
import {ActorsService} from '../actors.service';
import {Actor} from '../Actor';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'media-actors-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class ActorDetailComponent implements OnInit {

  private actor: Actor;

  constructor(private actorsService: ActorsService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.actor = this.actorsService.getActor(Number(params.id));
    });

    /*
     Of rewritten als normale functie:
     hier moet je rekening houden dat de callsight ergens in het angular framework ligt, waardoor this niet bruikbaar is.
     We kunnen de lexical this capturen met let that = this;
     */
     //
     // const that = this;
     // // tslint:disable-next-line:only-arrow-functions
     // this.route.params.subscribe(function(params: Params) {
     //   that.actor = that.actorsService.getActor(Number(params.id));
     // });
  }


}
