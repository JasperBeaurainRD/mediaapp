import { Component, OnInit } from '@angular/core';
import {Actor} from '../Actor';
import {ActorsService} from '../actors.service';

@Component({
  selector: 'media-actors-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class ActorsIndexComponent implements OnInit {

  private actors: Actor[];

  constructor(private actorsService: ActorsService) { }

  ngOnInit() {
    this.actors = this.actorsService.getActors();
  }

}
