import {Component, OnInit, ViewChild} from '@angular/core';
import {ActorsService} from '../actors.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'media-actors-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateActorComponent implements OnInit {

  @ViewChild('actorForm', {static: false}) actorForm: NgForm;

  constructor(private actorsService: ActorsService, private router: Router) { }

  ngOnInit() {  }

  private save() {
    this.actorsService.addActor(this.actorForm.value);
    this.router.navigate(['/actors']);
  }
}
