import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ActorsOverviewComponent} from './overview/overview.component';
import {ActorsIndexComponent} from './index/index.component';
import {ActorDetailComponent} from './detail/detail.component';
import {CreateActorComponent} from './create/create.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [
    ActorsOverviewComponent,
    ActorsIndexComponent,
    ActorDetailComponent,
    CreateActorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
  ]
})
export class ActorsModule { }
